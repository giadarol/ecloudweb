# LHC follwup

---

<p>Here we collect notes and plots from the <strong>followup of e-cloud observations at the LHC</strong>.&nbsp;</p>

<p>The <strong>material</strong> is organized by year:</p>

<ul>
	<li><strong><a href="https://cernbox.cern.ch/index.php/s/9Lota9RYxIcOB4I">Run 2018</a></strong></li>
	<li><strong><a href="https://cernbox.cern.ch/index.php/s/Pv8iE0HYHQrZTnF">Run 2017</a></strong></li>
	<li><strong><a href="https://cernbox.cern.ch/index.php/s/GjamdidVIbx6Cmx">Run 2016</a></strong></li>
	<li><strong><a href="https://cernbox.cern.ch/index.php/s/ad8vzNSusb3zYSJ">Run 2015</a></strong></li>
</ul>

![png](ccc.jpg)
