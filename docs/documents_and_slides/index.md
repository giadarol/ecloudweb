# Electron cloud studies

---

This page collects resources on the differet activities of the working group, organized in the following cathegories:

 * [Buildup and heat loads in the arc beam screens](#lhc-buildup-and-heat-loads-in-the-arc-beam-screens)
 * [Buildup in other devices](#buildup-in-other-devices)
 * [Instabilities and beam dynamics studies](#instabilities-and-beam-dynamics-studies)
 * [Beam-ion effects](#beam-ion-effects)
 * [Engineering change requests](#engineering-change-requests)

<p>Material from meetings in 2010-2013 is available <a href="https://project-ecloud-meetings.web.cern.ch/project-ecloud-meetings/meetings2010.htm">here</a>. Other resources from past studies are available <a href="http://ab-abp-rlc.web.cern.ch/ab-abp-rlc-ecloud/">here</a>.</p>

## LHC: buildup and heat loads in the arc beam screens


<table border="0" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col" style="width: 50%;"><strong>Title</strong></th>
			<th scope="col" style="width: 15%;"><strong>Author</strong></th>
			<th scope="col" style="width: 35%;"><strong>More info</strong></th>
		</tr>
	</thead>
	<tbody>
        <tr>
            <td><strong>Benchmarking of Surface Conditioning</strong></td>
            <td>M. Himmerlich</td>
            <td>Presentation at e-cloud meeting on 04 Dec 2020 (<a href="https://indico.cern.ch/event/977568/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Update on simulations with large gas densities: effect of solenoid</strong></td>
            <td>L. Mether</td>
            <td>Presentation at HL-LHC WP2 meeting on 24 Nov 2020 (<a href="https://indico.cern.ch/event/967065/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>LHC beam-induced heat loads: analysis of the beam screens surface and possible remedies</strong></td>
            <td>V. Petit</td>
            <td>Presentation at HL-LHC Annual meeting 2020 on 07 Oct 2020 (<a href="https://indico.cern.ch/event/937797/">indico page</a>)</td>
        </tr>
	    <tr>
            <td><strong>Effect of bunch intensity on the e-cloud heat load in the LHC arcs  (10'+5')</strong></td>
            <td>G. Skripka</td>
            <td>Presentation at ABP Information meeting on 11 Jun 2020 (<a href="https://indico.cern.ch/event/926256/">indico page</a>)</td>
        </tr>
	    <tr>
            <td><strong>Laser treatments for SEY reduction</strong></td>
            <td>M. Himmerlich</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
	    <tr>
            <td><strong>Ion energy spectrum for the LHC arcs</strong></td>
            <td>L. Mether</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Beam-induced heat loads on the LHC arc beam screens with different beam and machine configurations: experiments and comparison against simulations</strong></td>
			<td>G. Iadarola, G. Skripka et al.</td>
			<td><a href="http://cds.cern.ch/record/2705513">CERN-ACC-NOTE-2019-0057</a></td>
		</tr>
		<tr>
			<td><strong>Beam-induced heat loads on the beam screens of the HL-LHC arcs</strong></td>
			<td>G. Skripka and G. Iadarola</td>
			<td><a href="http://cds.cern.ch/record/2692753">CERN-ACC-NOTE-2019-0041</a></td>
		</tr>
		<tr>
			<td><strong>Calculations of the energy spectra of the impacting electrons</strong></td>
            <td>G. Skripka and G. Iadarola</td>
            <td>Presentation at Heat Load Task Force meeting on 4 Dec 2019 (<a href="https://indico.cern.ch/event/868591">indico page</a>)</td>
        <tr>
            <td><strong>Electron energy spectrum vs position in the beam screen (dipoles and drifts)</strong></td>
            <td>G. Skripka</td>
            <td>Presentation at e-cloud meeting on 22 Nov 2019 (<a href="https://indico.cern.ch/event/862794/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Status of the electron cloud simulations for HL-LHC: build-up and instabilities</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at 9th HL-LHC Collaboration meeting (Fermilab, USA) on 16 Oct 2019 (<a href="https://indico.cern.ch/event/806637/contributions/3573646/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Heat load task force: outcome of the analysis of beam screens and implications</strong></td>
            <td>M. Taborelli</td>
            <td>Presentation at LMC meeting on 2 Oct 2019 (<a href="https://indico.cern.ch/event/852492">indico page</a>)</td>
        </tr>		
		<tr>
            <td><strong>E-cloud heat loads on pumping slots
</strong></td>
            <td>G. Skripka and G. Iadarola</td>
            <td>Presentation at Heat Load Task Force meeting on 25 Sep 2019 (<a href="https://indico.cern.ch/event/850469">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Update on LHC beam screen analysis</strong></td>
            <td>V. Petit</td>
            <td>Presentation at e-cloud meeting on 14 Aug 2019 (<a href="https://indico.cern.ch/event/835473/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Follow-up on Furman-Pivi simulations</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at e-cloud meeting on 20 Aug 2019 (<a href="https://indico.cern.ch/event/840676/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Furman-Pivi model for secondary electron emission in PyECLOUD</strong></td>
            <td>E. Wulff</td>
            <td>Presentation at e-cloud meeting on 10 Jul 2019 (<a href="https://indico.cern.ch/event/830024/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Implementation and benchmarking of the Furman-Pivi model for Secondary Electron Emission in the PyECLOUD simulation code</strong></td>
			<td>E. Wulff and G. Iadarola</td>
			<td><a href="http://cds.cern.ch/record/2683285">CERN-ACC-NOTE-2019-0029</a></td>
		</tr>
		<tr>
			<td><strong>Comparison of electron cloud build-up simulations against heat load measurements for the LHC arcs with different beam configurations</strong></td>
			<td>G. Skripka et al.</td>
			<td>Contribution at IPAC19 (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2019/papers/wepts051.pdf">proceedings</a>)</td>
		</tr>	
		<tr>
			<td><strong>Simulation study on the impact of insulators on the e-cloud buildup</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at Heat Load Task Force meeting on 8 May 2019 (<a href="https://indico.cern.ch/event/819145">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Modelling of a thin insulating layer in PyECLOUD</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 5 Apr 2019 (<a href="https://indico.cern.ch/event/790354/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Investigation on secondary electron energy spectrum</strong></td>
			<td>E. Wulff</td>
			<td>Presentation&nbsp;at e-cloud meeting on 5 Apr 2019 (<a href="https://indico.cern.ch/event/790354/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Scaling of ecloud effects with bunch population</strong></td>
			<td>G. Skripka</td>
			<td>Presentation at the HiLumi WP2 Meeting on 5 Mar 2019&nbsp;(<a href="https://indico.cern.ch/event/800616/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>A look at low-energy electron modelling</strong></td>
			<td>E. Wulff</td>
			<td>Presentation&nbsp;at e-cloud meeting on 22 Feb 2019 (<a href="https://indico.cern.ch/event/790352/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on LHC arc heat load studies</strong></td>
			<td>G. Skripka</td>
			<td>Presentation&nbsp;at e-cloud meeting on 22 Feb 2019 (<a href="https://indico.cern.ch/event/790352/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>HL-LHC filling schemes: possible optimization</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at the HiLumi WP2 Meeting on 29 Jan 2019&nbsp;(<a href="https://indico.cern.ch/event/788818">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud and heat loads</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at the 9th LHC Operation Workshop, Evian, Jan 2019 (<a href="https://indico.cern.ch/event/751857/timetable/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on comparison of MD data against simulations</strong></td>
			<td>G. Skripka</td>
			<td>Presentation&nbsp;at e-cloud meeting on 2 Nov 2018 (<a href="https://indico.cern.ch/event/754131/">indico</a><a href="https://indico.cern.ch/event/735937/">&nbsp;</a><a href="https://indico.cern.ch/event/766100/">page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on the LHC heat-load and projection for HL-LHC</strong></td>
			<td>G. Iadarola</td>
			<td>8th HL-LHC collaboration meeting Oct 2018 (<a href="https://indico.cern.ch/event/742082/contributions/3072186/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on laboratory measurements on LHC beam screens</strong></td>
			<td>V. Petit</td>
			<td>Presentation&nbsp;at e-cloud meeting on 14 Sep 2018 (<a href="https://indico.cern.ch/event/754131/">indico</a><a href="https://indico.cern.ch/event/735937/">&nbsp;</a><a href="https://indico.cern.ch/event/754131/">page</a>)</td>
		</tr>
		<tr>
			<td><strong>Head loads in Run 1 and Run 2</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at LMC meeting 12 Sep 2018 (<a href="https://indico.cern.ch/event/756727/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on buildup simulations for the LHC arcs​ </strong>(effect a sextupolar error, SEY tables from lab measurements)</td>
			<td>L. Bitsikokos</td>
			<td>Presentation&nbsp;at e-cloud meeting on 5 Sep 2018 (<a href="https://indico.cern.ch/event/754131/">indico</a><a href="https://indico.cern.ch/event/735937/"> </a><a href="https://indico.cern.ch/event/754131/">page</a>)</td>
		</tr>
		<tr>
			<td><strong>Heat loads in the LHC arcs: observations and comparison against models</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at LMC meeting 29 Aug 2018 (<a href="https://indico.cern.ch/event/753301/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>e-cloud and heat load in the LHC arcs&nbsp;</strong></td>
			<td>G. Iadarola</td>
			<td>ABP forum 12 Jul 2018 (<a href="https://indico.cern.ch/event/740046">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Impact of the Secondary Emission Model on e-cloud build-up (50 ns vs 25 ns)</strong></td>
			<td>L. Bitsikokos</td>
			<td>Presentation&nbsp;at e-cloud meeting on 6 Jul 2018 (<a href="https://indico.cern.ch/event/735937/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Heat loads with different bunch spacings during high-beta* run</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 6 Jul 2018 (<a href="https://indico.cern.ch/event/735937/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Heat-Load budget in the arcs at 7TeV</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at&nbsp;2nd LHC Run-III Configuration Meeting (<a href="https://indico.cern.ch/event/732916/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Buildup simulations for the LHC arcs (25 ns vs 8b+4e)</strong></td>
			<td>G. Skripka</td>
			<td>Presentation&nbsp;at e-cloud meeting on 22 Jun 2018 (<a href="https://indico.cern.ch/event/735935/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>E-cloud Effects in CERN accelerator Complex</strong></td>
			<td>G. Rumolo</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>An overview on heat loads in the LHC</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Investigating the role of photoemission in the e-cloud formation at the LHC</strong></td>
			<td>P. Dijkstal</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>How does a cryogenic system cope with e-cloud induced heat load? what can we learn from heat load measurements (including evolution, transients, etc)?</strong></td>
			<td>B. Bradu</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Dynamic pressure related to electron cloud during Run2 machine operation in the LHC</strong></td>
			<td>C. Yin Vallgren</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Characterisation of beam screens extracted from LHC magnets</strong></td>
			<td>V. Petit</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Secondary Emission Models in e-cloud buildup simulations: from the lab to the code&nbsp;</strong></td>
			<td>L. Bitsikokos (presented by L. Sabato)</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>LHC beam screens: cryo status for 2018</strong></td>
			<td>B. Bradu</td>
			<td>Presentation&nbsp;at e-cloud meeting on 23 Mar 2018 (<a href="https://indico.cern.ch/event/714948/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>First results from PyECLOUD simulations with measured SEY curves</strong></td>
			<td>L. Bitsikokos</td>
			<td>Presentation&nbsp;at e-cloud meeting on 23 Mar 2018 (<a href="https://indico.cern.ch/event/714948/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Buildup simulations with 25 ns and 50 ns beams for large SEY</strong></td>
			<td>L. Sabato</td>
			<td>Presentation&nbsp;at e-cloud meeting on 23 Mar 2018 (<a href="https://indico.cern.ch/event/714948/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Meetings of the LHC Heat Load Task Force</strong></td>
			<td>L. Tavian et al.</td>
			<td><a href="https://indico.cern.ch/category/9684/">Indico cathegory</a></td>
		</tr>
		<tr>
			<td><strong>Analysis of the beam induced heat loads on the LHC arc beam screens during Run 2</strong></td>
			<td>G. Iadarola, G. Rumolo, et al.</td>
			<td><a href="https://cds.cern.ch/record/2298915">CERN-ACC-NOTE-2017-0066</a></td>
		</tr>
		<tr>
			<td><strong>Simulation studies on the electron cloud build-up in the elements of the LHC Arcs at 6.5 TeV</strong></td>
			<td>P. Dijkstal et al.</td>
			<td><a href="https://cds.cern.ch/record/2289940">CERN-ACC-NOTE-2017-0057</a></td>
		</tr>
		<tr>
			<td><strong>Electron cloud buildup studies for the LHC</strong></td>
			<td>P. Dijkstal</td>
			<td>Master thesis and "proposal work" (<a href="https://cernbox.cern.ch/index.php/s/NQfWxjXCG5hlv1t">pdf</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of heat loads on LHC arc beam-screens</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at LMC meeting on 30 Aug 2017 (<a href="https://indico.cern.ch/event/660465/contributions/2694376/attachments/1510870/2375503/006_LMC_heat_loads.pptx">slides</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of thermal loads by the cryogenics team</strong></td>
			<td>B. Bradu, K.&nbsp;Brodzinski</td>
			<td>Presentation&nbsp;at e-cloud meeting on 25 Aug 2017 (<a href="https://indico.cern.ch/event/660465/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of the heat load differences between sectors</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 25 Aug 2017 (<a href="https://indico.cern.ch/event/660465/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Impedance considerations on the measured heat loads</strong></td>
			<td>B. Salvant and F. Giordano</td>
			<td>Presentation&nbsp;at e-cloud meeting on 25 Aug 2017 (<a href="https://indico.cern.ch/event/660465/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Test with 50 ns beams at 6.5 TeV</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at the LMC meeting 26 Jul 2017 (<a href="https://indico.cern.ch/event/656121/">indico page</a>, <a href="https://indico.cern.ch/event/660465/contributions/2694376/attachments/1510297/2354937/009elcoud_considerations_.pptx">slides</a>)</td>
		</tr>
		<tr>
			<td><strong>Summary of the scrubbing run</strong></td>
			<td>L. Mether</td>
			<td>Presentation&nbsp;at e-cloud meeting on 14 Jul 2017 (<a href="https://indico.cern.ch/event/650342/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of heat loads during scrubbing run and intensity ramp up </strong>(includes also: global plots for the full run two, analysis of instrumented cells, some comparison against 2012, differences B1-B2)</td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 14 Jul 2017 (<a href="https://indico.cern.ch/event/650342/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Cryogenics observations and analysis</strong></td>
			<td>B. Bradu</td>
			<td>Presentation&nbsp;at e-cloud meeting on 14 Jul 2017 (<a href="https://indico.cern.ch/event/650342/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update from lab measurements</strong></td>
			<td>V. Petit</td>
			<td>Presentation&nbsp;at e-cloud meeting on 14 Jul 2017 (<a href="https://indico.cern.ch/event/650342/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Scrubbing run summary (LMC)</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at the LMC meeting on 14 June 2017 (<a href="https://ecloudwg.web.cern.ch/_site/files/files/material/005_scrubbing_summary_LMC.pptx">slides</a>)</td>
		</tr>
		<tr>
			<td><strong>Scrubbing run summary (LBOC)</strong></td>
			<td>L. Mether</td>
			<td>Presentation at the LBOC meeting on 13 June 2017 (<a href="https://ecloudwg.web.cern.ch/_site/files/files/material/005_scrubbing_summary_LMC.pptx">i</a><a href="https://indico.cern.ch/event/645898/">ndico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud effects at the LHC and LHC injectors</strong></td>
			<td>G. Rumolo</td>
			<td>Presentation at the IPAC16 conference (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2017/papers/moza1.pdf#search=%20domain%3Daccelconf%2Eweb%2Ecern%2Ech%20%20%2Bauthor%3A%22iadarola%22%20%20url%3Aaccelconf%2Fipac2017%20FileExtension%3Dpdf%20%2Durl%3Aabstract%20%2Durl%3Aaccelconf%2Fjacow">proceeding</a>)</td>
		</tr>
		<tr>
			<td><strong>Impact and mitigation of electron cloud effects in the operation of the Large Hadron Collider</strong></td>
			<td>G. Iadarola et al.</td>
			<td>Contribution at&nbsp;the IPAC16 conference (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2017/papers/tupva019.pdf">proceeding</a>)</td>
		</tr>
		<tr>
			<td><strong>SEY and XPS measurements on beam screens extracted from the LHC</strong></td>
			<td>V. Petit</td>
			<td>Presentation&nbsp;at e-cloud meeting on 12 Apr 2017 (<a href="https://indico.cern.ch/event/630626/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud build-up studies for the LHC&nbsp;</strong>(includes info on heat load recalculation and buildup sensitivity studies)</td>
			<td>P. Dijkstal</td>
			<td>Presentation&nbsp;at e-cloud meeting on 31 Mar 2017 (<a href="https://indico.cern.ch/event/605497/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Heat load analysis: observations with different filling schemes&nbsp;</strong>(summarizes present knowledge on heat load difference between sectors)</td>
			<td>G. Iadarola and P. Dijkstal</td>
			<td>Presentation at e-cloud meeting on 24 Feb 2017 (<a href="https://indico.cern.ch/event/615085/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Scrubbing in isolated dipoles</strong></td>
			<td>G. Iadarola</td>
			<td>Update&nbsp;at e-cloud meeting on 24 Feb 2017 (<a href="https://indico.cern.ch/event/615085/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>MD421: Electron cloud studies on 25 ns beam variants&nbsp;</strong></td>
			<td>G. Rumolo et al.</td>
			<td>MD note&nbsp;<a href="https://cds.cern.ch/record/2260998?ln=en">CERN-ACC-NOTE-2017-0028</a></td>
		</tr>
		<tr>
			<td><strong>MD468: Electron Cloud Reference Fills and Dependence on the Bunch Intensity</strong></td>
			<td>P. Dijkstal et al.</td>
			<td>MD note&nbsp;<a href="https://cds.cern.ch/record/2260999?ln=en">CERN-ACC-NOTE-2017-0029</a></td>
		</tr>
		<tr>
			<td><strong>Are the electron clouds clearing?</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at the LHC Performance Workshop, Chamonix 2017 (<a href="https://indico.cern.ch/event/580313/timetable/">indico page</a>, <a href="https://indico.cern.ch/event/580313/contributions/2366334/attachments/1400136/2136662/023_chamonix2017_gi_ecloud_sh.pptx">slides</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud in 2016: cloudy or clear?</strong></td>
			<td>L. Mether</td>
			<td>Presentation at 7th Evian Workshop, Dec 2016 (<a href="https://indico.cern.ch/event/578001/timetable/">indico page</a>, <a href="https://indico.cern.ch/event/578001/contributions/2366388/attachments/1388345/2113869/006_Evian_ecloud_Lotta.pptx">slides</a>, <a href="https://indico.cern.ch/event/578001/contributions/2366388/attachments/1388345/2281023/Evian2016_ecloud_paper.pdf">proceeding</a>)</td>
		</tr>
		<tr>
			<td><strong>Filling schemes and e-cloud constraints</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at 7th Evian Workshop, Dec 2016 (<a href="https://indico.cern.ch/event/578001/timetable/">indico page</a>,&nbsp;<a href="https://indico.cern.ch/event/578001/contributions/2367030/attachments/1388974/2115359/010_ecloud_session8.pptx">slides</a>,&nbsp;<a href="https://indico.cern.ch/event/578001/contributions/2367030/attachments/1388974/2220835/ecloud_evian2016.pdf">proceeding</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of cryogenics data&nbsp;</strong>(includes comparison with respect to 25 ns run in 2012)</td>
			<td>B. Bradu</td>
			<td>Presention&nbsp;at e-cloud meeting on 12 Dec 2016 (<a href="https://indico.cern.ch/event/592369/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of data from instrumented cells</strong></td>
			<td>P. Dijkstal</td>
			<td>Presentation&nbsp;at e-cloud meeting on 12 Dec 2016 (<a href="https://indico.cern.ch/event/592369/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on scrubbing observations</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at the LMC meeting on 9 Nov 2016 (<a href="https://cernbox.cern.ch/index.php/s/88nfBr1mILgtYvT">slides</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on LHC e-cloud build up studies</strong> (scrubbing evolution; dose evaluation, results from MDs; matching experimental data with models; bunch-by-bunch tune measurements at 450 GeV and 6.5 TeV)</td>
			<td>G. Iadarola, P. Dijkstal, L. Carver</td>
			<td>Presentation&nbsp;at e-cloud meeting on 3 Nov 2016 (<a href="https://indico.cern.ch/event/579849/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on LHC e-cloud observables and machine studies</strong></td>
			<td>L. Mether</td>
			<td>Presentation&nbsp;at e-cloud meeting on 6 Oct 2016 (<a href="https://indico.cern.ch/event/570152/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Cryo and vacuum updates </strong>(following beam screen warm-up in selected cells)</td>
			<td>B. Bradu, K.&nbsp;Brodzinski, C.&nbsp;Yin Vallgren</td>
			<td>Presentation&nbsp;at e-cloud meeting on 6 Oct 2016 (<a href="https://indico.cern.ch/event/570152/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>News from LHC&nbsp;</strong>(heat loads in the arcs and in other devices, comparison heat load vs power loss from RF, e-cloud pattern on losses in collisions, heat load vs bunch intensity during a long fill, accumulated dose)</td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 13 Jul 2016 (<a href="https://indico.cern.ch/event/547898/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Sector-by-sector beam loss observations</strong></td>
			<td>D. Mirarchi</td>
			<td>Presentation&nbsp;at e-cloud meeting on 13 Jul 2016 (<a href="https://indico.cern.ch/event/547898/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on heat load measurements and feedforward</strong></td>
			<td>B. Bradu, K.&nbsp;Brodzinski</td>
			<td>Presentation&nbsp;at e-cloud meeting on 13 Jul 2016 (<a href="https://indico.cern.ch/event/547898/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>News from LHC</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 3 Jun 2016 (<a href="https://indico.cern.ch/event/536930/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Heat load measurements in 2016</strong></td>
			<td>E. Rogez,&nbsp;K.&nbsp;Brodzinski</td>
			<td>Presentation&nbsp;at e-cloud meeting on 3 Jun 2016 (<a href="https://indico.cern.ch/event/536930/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Cryogenic system in 2016</strong></td>
			<td>E. Rogez,&nbsp;K.&nbsp;Brodzinski</td>
			<td>Presentation&nbsp;at e-cloud meeting on 17 Mar 2016 (<a href="https://indico.cern.ch/event/507355/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron Cloud Effects</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at Chamonix 2016 workshop (<a href="https://indico.cern.ch/event/448109/timetable/?view=standard">indico page</a>,&nbsp;<a href="https://indico.cern.ch/event/448109/contributions/1942042/attachments/1216243/1777100/007_ecloud_Cham2016_GI.pptx">slides</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud effects</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at 6th Evian Workshop, Dec 2015 (<a href="https://indico.cern.ch/event/434129/timetable/#all.detailed">indico page</a>,&nbsp;<a href="https://indico.cern.ch/event/434129/contributions/1917210/attachments/1205675/1756872/016_ecloud_Evian2015_GI.pptx">slides</a>,&nbsp;<a href="https://indico.cern.ch/event/434129/contributions/1917210/attachments/1205675/1837702/ecloud_evian2015.pdf">proceeding</a>)</td>
		</tr>
		<tr>
			<td><strong>Follow up from e-cloud team</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 13 Oct 2015 (<a href="https://indico.cern.ch/event/452707/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of beam losses in the arcs</strong></td>
			<td>D. Mirarchi</td>
			<td>Presentation&nbsp;at e-cloud meeting on 13 Oct 2015 (<a href="https://indico.cern.ch/event/452707/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Follow up from cryogenics</strong></td>
			<td>G. Ferlin</td>
			<td>Presentation&nbsp;at e-cloud meeting on 13 Oct 2015 (<a href="https://indico.cern.ch/event/452707/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Follow up from vacuum</strong></td>
			<td>C. Yin Vallgren</td>
			<td>Presentation&nbsp;at e-cloud meeting on 13 Oct 2015 (<a href="https://indico.cern.ch/event/452707/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Experience with 25 ns beam operation in the LHC</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation&nbsp;at e-cloud meeting on 25 Sep 2015 (<a href="https://indico.cern.ch/event/446455/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Input from cryogenics and vacuum</strong></td>
			<td>G. Ferlin, C. Yin Vallgren, et al.</td>
			<td>Presentation&nbsp;at e-cloud meeting on 25 Sep 2015 (<a href="https://indico.cern.ch/event/446455/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Heat load analysis triplet &amp; SAMs</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at LBOC meeting on 28 Oct 2014 (<a href="https://indico.cern.ch/event/348522/">indico page</a>)</td>
		</tr>
	</tbody>
</table>

## Buildup in other devices

<table border="0" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col" style="width: 50%;"><strong>Title</strong></th>
			<th scope="col" style="width: 15%;"><strong>Author</strong></th>
			<th scope="col" style="width: 35%;"><strong>More info</strong></th>
		</tr>
	</thead>
	<tbody>
        <tr>
            <td><strong>Ecloud build-up simulations in the crab cavities (15'+5')</strong></td>
            <td>L. Giacomel</td>
            <td>Presentation at ABP Information meeting on 12 Nov 2020 (<a href="https://indico.cern.ch/event/974183/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Ecloud build-up simulations in the crab cavities</strong></td>
            <td>L. Giacomel</td>
            <td>Presentation at HL-LHC WP2 meeting on 10 Nov 2020 (<a href="https://indico.cern.ch/event/961360/">indico page</a>)</td>
        </tr>
	    <tr>
            <td><strong>E-cloud in the IR BPMs</strong></td>
            <td>G. Skripka</td>
            <td>Presentation at HL-LHC WP2 meeting on 07 Jul 2020 (<a href="https://indico.cern.ch/event/924097/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>E-cloud in triplets with Cu-LESS SEY curves</strong></td>
            <td>G. Skripka</td>
            <td>Presentation at HL-LHC WP2 meeting on 07 Jul 2020 (<a href="https://indico.cern.ch/event/924097/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Heat loads on HL-LHC triplet BPMs</strong></td>
            <td>G. Skripka</td>
            <td>Presentation at e-cloud meeting on 16 Apr 2020 (<a href="https://indico.cern.ch/event/895303/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Analysis of measurements from the LHC Vacuum Pilot Sector</strong></td>
            <td>E. Buratin</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
	    <tr>
            <td><strong>Development of WARP simulations for 3D RF structures</strong></td>
            <td>L. Giacomel</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Electron cloud build-up simulations in the two-beam common chamber of the HL-LHC TDIS with non-uniform surface properties</td></strong>
			<td>G. Skripka et al.</td>
			<td>Contribution at IPAC20 (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2019/papers/wepts052.pdf">proceedings</a>)</td>
		</tr>
        <tr>
            <td><strong>A study on the e-cloud saturation mechanism</strong></td>
            <td>L. Giacomel</td>
            <td>Presentation at e-cloud meeting on 10 May 2019 (<a href="https://indico.cern.ch/event/811014/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Optimization of the position of the pumping slots for the HL-LHC D2 beam screen</strong></td>
            <td>G. Skripka</td>
            <td>Presentation at HSC meeting on 29 Apr 2019 (<a href="https://indico.cern.ch/event/812709/contributions/3388098/">indico page</a>)</td>
        </tr>			
		<tr>
			<td><strong>Update on SPS "SEY drum": measurements on copper sample</strong></td>
			<td>V. Petit</td>
			<td>Presentation&nbsp;at e-cloud meeting on 5 Apr 2019 (<a href="https://indico.cern.ch/event/790354/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>In situ SEY measurements at the SPS</strong></td>
			<td>V. Petit</td>
			<td>Presentation&nbsp;at e-cloud meeting on 30 Nov 2018 (<a href="https://indico.cern.ch/event/754131/">indico</a><a href="https://indico.cern.ch/event/735937/">&nbsp;</a><a href="https://indico.cern.ch/event/754131/">page</a>)</td>
		</tr>
		<tr>
			<td><strong>SPS e-cloud detectors after LS2</strong></td>
			<td>H. Neupert</td>
			<td>Presentation&nbsp;at e-cloud meeting on 30 Nov 2018 (<a href="https://indico.cern.ch/event/754131/">indico</a><a href="https://indico.cern.ch/event/735937/">&nbsp;</a><a href="https://indico.cern.ch/event/754131/">page</a>)</td>
		</tr>
		<tr>
			<td><strong>Buildup simulation studies on circular chambers</strong></td>
			<td>E. Wulff</td>
			<td>Presentation&nbsp;at e-cloud meeting on 2 Nov 2018 (<a href="https://indico.cern.ch/event/754131/">indico</a><a href="https://indico.cern.ch/event/735937/">&nbsp;</a><a href="https://indico.cern.ch/event/754131/">page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud studies for the LHC TDI and HL-LHC TDIS&nbsp;</strong></td>
			<td>G. Skripka and G. Iadarola</td>
			<td><a href="http://cds.cern.ch/record/2636658/">CERN-ACC-NOTE-2018-0060</a></td>
		</tr>
		<tr>
			<td><strong>e-cloud buildup in a collimator</strong></td>
			<td>E. Wulff</td>
			<td>Presentation&nbsp;at e-cloud meeting on 5 Sep 2018 (<a href="https://indico.cern.ch/event/754131/">indico</a><a href="https://indico.cern.ch/event/735937/">&nbsp;</a><a href="https://indico.cern.ch/event/754131/">page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud build up in two-beam regions for HL-LHC, heat load and vacuum aspects</strong></td>
			<td>G. Skripka (presented by A. Romano)</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Preliminary results obtained with the LHC Vacuum Pilot Sector</strong></td>
			<td>E. Buratin</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Buildup studies for the LHC TDIS</strong></td>
			<td>G. Skripka</td>
			<td>Presention&nbsp;at e-cloud meeting on 27 Feb 2018 (<a href="https://indico.cern.ch/event/707491/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Beam-induced heat loads on the beam screens of the inner triplets for the HL-LHC</strong></td>
			<td>G. Skripka and G. Iadarola</td>
			<td><a href="http://cds.cern.ch/record/2305245">CERN-ACC-NOTE-2018-0009</a></td>
		</tr>
		<tr>
			<td><strong>E-cloud buildup simulations for the LHC TDI</strong></td>
			<td>G. Skripka</td>
			<td>Presention&nbsp;at e-cloud meeting on 24 Feb 2017 (<a href="https://indico.cern.ch/event/615085/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Simulation studies of e-cloud in LHC LSS and its effect on dynamic vacuum</strong></td>
			<td>J. Sopousek&nbsp;</td>
			<td>Presention&nbsp;at e-cloud meeting on 13 Jul 2017 (<a href="https://indico.cern.ch/event/547898/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Beam induced heat loads on the beam-screens of the twin-bore magnets in the IRs of the HL-LHC</strong></td>
			<td>G. Iadarola et al.</td>
			<td><a href="http://cds.cern.ch/record/2217217">CERN-ACC-2016-0112</a></td>
		</tr>
	</tbody>
</table>

## Instabilities and beam dynamics studies

<table border="0" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col" style="width: 50%;"><strong>Title</strong></th>
			<th scope="col" style="width: 15%;"><strong>Author</strong></th>
			<th scope="col" style="width: 35%;"><strong>More info</strong></th>
		</tr>
	</thead>
	<tbody>
        <tr>
            <td><strong>Application of Vlasov method to instabilities driven by e-cloud in the SPS dipoles</strong></td>
            <td>L. Mether</td>
            <td>Presentation at e-cloud meeting on 04 Dec 2020 (<a href="https://indico.cern.ch/event/977568/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Tune shifts at the SPS</strong></td>
            <td>L. Mether</td>
            <td>Presentation at ABP injectors WG meeting on 27 Nov 2020 (<a href="https://indico.cern.ch/event/968538/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Coupled bunch stability and tune shifts</strong></td>
            <td>L. Mether</td>
            <td>Presentation at HL-LHC WP2 meeting on 24 Nov 2020 (<a href="https://indico.cern.ch/event/967065/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Incoherent effects from e-cloud with SixTrackLib</strong></td>
            <td>K. Paraschou</td>
            <td>Presentation at HL-LHC WP2 meeting on 24 Nov 2020 (<a href="https://indico.cern.ch/event/967065/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Effect of the synchrotron tune on e-cloud instabilities</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at LMC meeting 14 Oct 2020 (<a href="https://indico.cern.ch/event/965509">indico page</a>)</td>
		</tr>
        <tr>
            <td><strong>Coherent tune shifts from coupled-bunch simulations for SPS and LHC</strong></td>
            <td>L. Mether</td>
            <td>Presentation at e-cloud meeting on 09 Oct 2020 (<a href="https://indico.cern.ch/event/962349/">indico page</a>)</td>
        </tr>    
        <tr>
            <td><strong>Application of Vlasov method to instabilities driven by e-cloud in the LHC dipoles</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at e-cloud meeting on 09 Oct 2020 (<a href="https://indico.cern.ch/event/962349/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>E-cloud instabilities at LHC and HL-LHC - numerical simulations</strong></td>
            <td>L. Sabato</td>
            <td>Presentation at HSC Section meeting on 21 Sep 2020 (<a href="https://indico.cern.ch/event/952122/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Simulations of incoherent effects from e-cloud</strong></td>
            <td>K. Paraschou</td>
            <td>Presentation at e-cloud meeting on 01 Sep 2020 (<a href="https://indico.cern.ch/event/948277/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>A Vlasov approach to study transverse instabilities driven by electron clouds</strong></td>
			<td>G. Iadarola et al.</td>
			<td><a href="http://cds.cern.ch/record/2740683">CERN-ACC-NOTE-2020-0054</a></td>
		</tr>
		<tr>
		<td><strong>Numerical simulations studies on single-bunch instabilities driven by electron clouds at the LHC </strong></td>
			<td>L. Sabato et al.</td>
			<td><a href="http://cds.cern.ch/record/2733028">CERN-ACC-NOTE-2020-0050</a></td>
		</tr>
		<tr>
			<td><strong>Linearized method for the study of transverse instabilities driven by electron clouds </strong></td>
			<td>G. Iadarola et al.</td>
			<td><a href="http://dx.doi.org/10.1103/PhysRevAccelBeams.23.081002">Phys. Rev. Accel. Beams 23, 081002</a> – Published 26 August 2020&nbsp;</td>
		</tr>
        <tr>
            <td><strong>Linearised method for e-cloud instabilities</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at HL-LHC WP2 meeting on 07 Jul 2020 (<a href="https://indico.cern.ch/event/924097/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Self-consistent electron cloud simulations to study coupled bunch instabilities</strong></td>
			<td>L. Mether</td>
			<td>Presentation at HPC User Meeting on 09 Jun 2020 (<a href="https://indico.cern.ch/event/867459">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>LHC e-cloud observables</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at HSC Section meeting on 25 May 2020 (<a href="https://indico.cern.ch/event/919193/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Ecloud in DELPHI</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at HSC Section meeting on 20 Apr 2020 (<a href="https://indico.cern.ch/event/904845/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Beam stability with e-cloud for bunch intensities below 2.3e11</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
 		<tr>
            <td><strong>MO settings at injection vs. beam parameters</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at LHC Run 3 configuration meeting on 27 Mar 2020 (<a href="https://indico.cern.ch/event/902528/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Simplified modelling of e-cloud instabilities</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at HSC Section meeting on 17 Feb 2020 (<a href="https://indico.cern.ch/event/883740/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>e-cloud MDs</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at LHC MD day on 28 Jan 2020 (<a href="https://indico.cern.ch/event/867177/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Symplectic kicks from an electron cloud pinch (5'+5')</strong></td>
            <td>K. Paraschou</td>
            <td>Presentation at ABP Information meeting on 23 Jan 2020 (<a href="https://indico.cern.ch/event/880340/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Status of the studies on electron cloud incoherent effects</strong></td>
			<td>K. Paraschou</td>
			<td>Presentation at HL-LHC WP2 meeting on 10 Dec 2019 (<a href="https://indico.cern.ch/event/863723/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud stability: Single bunch effects</strong></td>
			<td>L. Sabato</td>
			<td>Presentation at HL-LHC WP2 meeting on 10 Dec 2019 (<a href="https://indico.cern.ch/event/863723/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud stability: Coupled bunch effects</strong></td>
			<td>L. Mether</td>
			<td>Presentation at HL-LHC WP2 meeting on 10 Dec 2019 (<a href="https://indico.cern.ch/event/863723/">indico page</a>)</td>
		</tr>
		<tr>
            <td><strong>Update on development for single-particle tracking with e-cloud</strong></td>
            <td>K. Paraschou</td>
            <td>Presentation at e-cloud meeting on 29 Nov 2019 (<a href="https://indico.cern.ch/event/862798/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Investigation on convergence properties: single pinch</strong></td>
            <td>L. Sabato</td>
            <td>Presentation at e-cloud meeting on 29 Nov 2019 (<a href="https://indico.cern.ch/event/862798/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Single bunch instabilities at injection energy: simulation studies</strong></td>
            <td>L. Sabato</td>
            <td>Presentation at e-cloud meeting on 22 Nov 2019 (<a href="https://indico.cern.ch/event/862794/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Single bunch instabilities at injection energy: indications from frequency analysis</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at e-cloud meeting on 22 Nov 2019 (<a href="https://indico.cern.ch/event/862794/">indico page</a>)</td>
        </tr>	
		<tr>
			<td><strong>Analysis of losses in collisions</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at LBOC meeting on 19 Nov 2019 (<a href="https://indico.cern.ch/event/859514/">indico page</a>)</td>
		</tr>
		<tr>
            <td><strong>Status of the electron cloud simulations for HL-LHC: build-up and instabilities</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at 9th HL-LHC Collaboration meeting (Fermilab, USA) on 16 Oct 2019 (<a href="https://indico.cern.ch/event/806637/contributions/3573646/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Incoherent electron cloud effects in the LHC</strong></td>
			<td>K. Paraschou</td>
			<td>Presentation at ICFA mini-workshop on mitigation of coherent instabilities on 27 Sep 2019 (<a href="https://indico.cern.ch/event/775147/contributions/3366449/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Beam lifetime in collision - LHC experience</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at HL-LHC WP2 meeting on 24 Sep 2019 (<a href="https://indico.cern.ch/event/850136">indico page</a>)</td>
		</tr>
        <tr>
            <td><strong>Effect of e-cloud magnetic field on proton and electron motion</strong></td>
            <td>L. Giacomel</td>
            <td>Presentation at e-cloud meeting on 20 Aug 2019 (<a href="https://indico.cern.ch/event/840676/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Symplectic kicks from an e-cloud pinch</strong></td>
            <td>K. Paraschou</td>
            <td>Presentation at e-cloud meeting on 10 May 2019 (<a href="https://indico.cern.ch/event/811014/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Modelling the interaction of a relativistic beam particle with an electron cloud</strong></td>
			<td>G. Iadarola</td>
			<td><a href="http://cds.cern.ch/record/2684858">CERN-ACC-NOTE-2019-0033</a></td>
		</tr>
		<tr>
			<td><strong>Interaction of a relativistic beam particle with an electron cloud</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at HSC section meeting on 19 Aug 2019 (<a href="https://indico.cern.ch/event/840690">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis on bunch-by-bunch beam losses at 6.5 TeV in the Large Hadron Collider</strong></td>
			<td>K. Paraschou et al.</td>
			<td>Contribution at IPAC19 (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2019/papers/mopmp029.pdf">proceedings</a>)</td>
		</tr>		
		<tr>
			<td><strong>Properties of the electromagnetic fields generated by a circular-symmetric e-cloud pinch in the ultra-relativistic limit</td></strong>
			<td>G. Iadarola</td>
			<td><a href="http://cds.cern.ch/record/2674759">CERN-ACC-NOTE-2019-0017</a></td>			
		</tr>
		<tr>
			<td><strong>Analysis electron motion within the beam</td></strong>
			<td>L. Sabato</td>
			<td>Presentation&nbsp;at e-cloud meeting on 22 Feb 2019 (<a href="https://indico.cern.ch/event/790352/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>New tools for the simulation of coupled bunch instabilities driven by electron cloud</strong></td>
			<td>G. Iadarola</td>
			<td>Presentation at&nbsp;Joint E-Cloud-PyHEADTAIL Meeting&nbsp;14 Dec 2018 (<a href="https://indico.cern.ch/event/776089/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Single bunch instability simulations: basic checks and convergence</strong></td>
			<td>L. Sabato</td>
			<td>Presentation&nbsp;at e-cloud meeting on 2 Nov 2018 (<a href="https://indico.cern.ch/event/766100/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Follow-up on instability simulation studies&nbsp;</strong></td>
			<td>L. Sabato</td>
			<td>Presentation&nbsp;at e-cloud meeting on 22 Jun 2018 (<a href="https://indico.cern.ch/event/735935/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud formation in CERN particle accelerators and its impact on the beam dynamics</strong></td>
			<td>A. Romano</td>
			<td><a href="https://cds.cern.ch/record/2652799">CERN-THESIS-2018-299</a></td>
		</tr>
		<tr>
			<td><strong>A global view on electron cloud instability simulations in the LHC and HL-LHC</strong></td>
			<td>A. Romano</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud buildup driving spontaneous vertical instabilities of stored beams in the Large Hadron Collider</strong></td>
			<td>A. Romano et al.</td>
			<td><a href="https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.21.061002">Phys. Rev. Accel. Beams 21, 061002</a> – Published 8 June 2018&nbsp;</td>
		</tr>
		<tr>
			<td><strong>Stability with e-cloud (HL-LHC)</strong></td>
			<td>A. Romano</td>
			<td>Presentation at the HL-LHC WP2 meeting 3 Oct 2017 (<a href="https://indico.cern.ch/event/668032/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Analysis of ADTObsBox data</strong></td>
			<td>L. Carver</td>
			<td>Presentation&nbsp;at e-cloud meeting on 14 Jul 2017 (<a href="https://indico.cern.ch/event/650342/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Macroparticle simulation studies of the LHC beam dynamics in the presence of electron cloud</strong></td>
			<td>A. Romano et al.</td>
			<td>Contribution at&nbsp;the IPAC16 conference (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2017/papers/tupva018.pdf#search=%20domain%3Daccelconf%2Eweb%2Ecern%2Ech%20%20%2Bauthor%3A%22iadarola%22%20%20url%3Aaccelconf%2Fipac2017%20FileExtension%3Dpdf%20%2Durl%3Aabstract%20%2Durl%3Aaccelconf%2Fjacow">proceeding</a>)</td>
		</tr>
		<tr>
			<td><strong>Evolution of python tools for the simulation of electron cloud effects</strong></td>
			<td>G. Iadarola et al.</td>
			<td>Contribution at&nbsp;the IPAC16 conference (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2017/papers/thpab043.pdf#search=%20domain%3Daccelconf%2Eweb%2Ecern%2Ech%20%20%2Bauthor%3A%22iadarola%22%20%20url%3Aaccelconf%2Fipac2017%20FileExtension%3Dpdf%20%2Durl%3Aabstract%20%2Durl%3Aaccelconf%2Fjacow">proceeding</a>)</td>
		</tr>
		<tr>
			<td><strong>Status update on e-cloud stability simulations for LHC </strong>(complete overiview of instability behavior at 450 GeV and 6.5 TeV, including effect of e-cloud in dipoles and/or quadrupoles, transverse damper, octupoles and chromaticity)</td>
			<td>A. Romano</td>
			<td>Presentation at&nbsp;Joint E-Cloud-PyHEADTAIL Meeting&nbsp;12 May 2017 (<a href="https://indico.cern.ch/event/638087/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Electron cloud instability studies for the LHC -&nbsp;status of PhD work&nbsp;</strong>(includes simulation results on instablities in Stable Beams, and first results on scans at injection and flat-top)</td>
			<td>A. Romano</td>
			<td>Presention&nbsp;at e-cloud meeting on 31 Mar 2017 (<a href="https://indico.cern.ch/event/605497/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on LHC e-cloud instabilities&nbsp;</strong>(includes tune shift estimates at 450 GeV and 6.5 TeV)</td>
			<td>A. Romano</td>
			<td>Presention&nbsp;at e-cloud meeting on 3 Nov 2016 (<a href="https://indico.cern.ch/event/579849/">indico page</a>).</td>
		</tr>
		<tr>
			<td><strong>Overview on beam dynamics simulations for LHC&nbsp;</strong>(instability thresholds, tune shifts, tune spreads)</td>
			<td>A. Romano</td>
			<td>Presention&nbsp;at e-cloud meeting on 30 Sep 2016 (<a href="https://indico.cern.ch/event/570147/">indico page</a>).</td>
		</tr>
		<tr>
			<td><strong>Update on tune shifts along the batches</strong></td>
			<td>L. Carver</td>
			<td>Presention&nbsp;at e-cloud meeting on 30 Sep 2016 (<a href="https://indico.cern.ch/event/570147/">indico page</a>).</td>
		</tr>
		<tr>
			<td><strong>Update on high energy tune footprint studies</strong></td>
			<td>A. Romano</td>
			<td>Presentation&nbsp;at e-cloud meeting on 3 Jun 2016 (<a href="https://indico.cern.ch/event/536930/">indico page</a>)</td>
		</tr>
	</tbody>
</table>

## Beam-ion effects
<table border="0" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col" style="width: 50%;"><strong>Title</strong></th>
			<th scope="col" style="width: 15%;"><strong>Author</strong></th>
			<th scope="col" style="width: 35%;"><strong>More info</strong></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><strong>Impact of electron-induced ionization on heat load and build-up</strong></td>
			<td>L. Mether</td>
			<td>Presentation at HL-LHC WP2 meeting on 10 Dec 2019 (<a href="https://indico.cern.ch/event/863723/">indico page</a>)</td>
		</tr>
        <tr>
            <td><strong>Multispecies simulations with cross-ionisation: investigation on numerical runaway</strong></td>
            <td>L. Mether</td>
            <td>Presentation at e-cloud meeting on 29 Nov 2019 (<a href="https://indico.cern.ch/event/862798/">indico page</a>)</td>
        </tr>
		<tr>
            <td><strong>Implementation of cross-species ionization in PyECLOUD</strong></td>
            <td>L. Mether</td>
            <td>Presentation at e-cloud meeting on 14 Aug 2019 (<a href="https://indico.cern.ch/event/835473/">indico page</a>)</td>
        </tr>
        <tr>
            <td><strong>Investigation of e-/ion dynamics for very high gas densities (16L2 regime)</strong></td>
            <td>L. Mether</td>
            <td>Presentation at e-cloud meeting on 10 May 2019 (<a href="https://indico.cern.ch/event/811014/">indico page</a>)</td>
        </tr>
		<tr>
			<td><strong>Multi-species electron-ion simulations and their application to the LHC</td>
			<td>L. Mether et al.</td>
			<td>Contribution at IPAC19 (<a href="http://accelconf.web.cern.ch/AccelConf/ipac2019/papers/wepts050.pdf">proceedings</a>)</td>
		</tr>
		<tr>
			<td><strong>Update on multi-species simulation studies: SMOG2</td></strong>
			<td>K. Poland</td>
			<td>Presentation&nbsp;at e-cloud meeting on 22 Feb 2019 (<a href="https://indico.cern.ch/event/790352/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Effect of ion field on the e-cloud buildup</strong></td>
			<td>K. Poland</td>
			<td>Presentation&nbsp;at e-cloud meeting on 14 Sep 2018 (<a href="https://indico.cern.ch/event/754131/">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Progress on the understanding of 16L2 events</strong></td>
			<td>L. Mether</td>
			<td>Presentation at the LBOC meeting on 31 Jul 2018 (<a href="https://indico.cern.ch/event/746500">indico page</a>)</td>
		</tr>
		<tr>
			<td><strong>Post-ecloud regime, challenges for multi-species simulations in beam dynamics</strong></td>
			<td>L. Mether</td>
			<td>Presentation at ECLOUD18 workshop (<a href="https://agenda.infn.it/conferenceTimeTable.py?confId=13351">indico page</a>)</td>
		</tr>	
	</tbody>
</table>


## Engineering change requests

<table border="0" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col"><strong>Title</strong></th>
			<th scope="col">EDMS link</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><strong>HL-LHC triplet BPMs</strong></td>
			<td><a href="https://edms.cern.ch/document/2394486/0.5">https://edms.cern.ch/document/2394486/0.5</a></td>
		</tr>
		<tr>
			<td><strong>LHC VELO Upgrade</strong></td>
			<td><a href="https://edms.cern.ch/document/LHC-X8VELO-EC-0001/0.1">https://edms.cern.ch/document/LHC-X8VELO-EC-0001/0.1</a></td>
		</tr>
		<tr>
			<td><strong>Engineering Specification for Crab Cavities</strong></td>
			<td><a href="https://edms.cern.ch/document/LHC-ACFDC-ES-0002/2.55">https://edms.cern.ch/document/LHC-ACFDC-ES-0002/2.55</a></td>
		</tr>
	    <tr>
			<td><strong>LHC electro-optical BPMs</strong></td>
			<td><a href="https://edms.cern.ch/document/2369610/0.9/">https://edms.cern.ch/document/2369610/0.9</a></td>
		</tr>
        <tr>
			<td><strong>LHC BGC</strong></td>
			<td><a href="https://edms.cern.ch/document/2363497/">https://edms.cern.ch/document/2363497</a></td>
		</tr>
		<tr>
			<td><strong>HL-LHC TCL*</strong></td>
			<td><a href="https://edms.cern.ch/document/2276600">https://edms.cern.ch/document/2276600</a></td>
		</tr>
		<tr>
			<td><strong>SMOG2</strong></td>
			<td><a href="https://edms.cern.ch/document/2085258">https://edms.cern.ch/document/2085258</a></td>
		</tr>
		<tr>
			<td><strong>Intensity measurements specs for HL-LHC</strong></td>
			<td><a href="https://edms.cern.ch/document/2227154/1">https://edms.cern.ch/document/2227154/1</a></td>
		</tr>
		<tr>
			<td><strong>LHC cryo instrumentation (local)</strong></td>
			<td><a href="https://edms.cern.ch/document/2045687">https://edms.cern.ch/document/2045687</a></td>
		</tr>
		<tr>
			<td><strong>LHC E-Cloud Investigation&nbsp;</strong></td>
			<td><a href="https://edms.cern.ch/document/2025993">https://edms.cern.ch/document/2025993</a></td>
		</tr>
		<tr>
			<td><strong>LHC TDIS injection dump</strong></td>
			<td><a href="https://edms.cern.ch/document/1936580">https://edms.cern.ch/document/1936580</a></td>
		</tr>
		<tr>
			<td><strong>LHC 11 T dipole</strong></td>
			<td><a href="https://edms.cern.ch/document/1995306">https://edms.cern.ch/document/1995306</a></td>
		</tr>
		<tr>
			<td><strong>LHC additional BLMs in 16L2 and 31L2 </strong></td>
			<td><a href="https://edms.cern.ch/document/1983534">https://edms.cern.ch/document/1983534</a>, <a href="https://edms.cern.ch/document/1984046">https://edms.cern.ch/document/1984046</a></td>
		</tr>
		<tr>
			<td><strong>SPS LSS3 upgrade</strong></td>
			<td><a href="https://edms.cern.ch/document/100058315">https://edms.cern.ch/document/100058315</a></td>
		</tr>
		<tr>
			<td><strong>SPS TIDVG</strong></td>
			<td><a href="https://edms.cern.ch/document/1700216">https://edms.cern.ch/document/1700216</a></td>
		</tr>
		<tr>
			<td><strong>SPS Slotted line kicker</strong></td>
			<td><a href="https://edms.cern.ch/document/1835539">https://edms.cern.ch/document/1835539</a></td>
		</tr>
	</tbody>
</table>
