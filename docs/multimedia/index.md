# Multimedia

---

<p>This page collects some video files showing the e-cloud dynamics in different accelerator components:</p>

<ul>
	<li><strong>LHC arc e-cloud <a href="https://cernbox.cern.ch/index.php/s/Bfj1hmPS34TO3qQ">buildup movies</a> </strong>(by A. Romano)</li>
	<li><strong>HL-LHC e-cloud <a href="https://cernbox.cern.ch/index.php/s/uXM3nyfZLVGErXY">buildup movies</a>&nbsp;</strong>(by G. Skripka&nbsp;and A. Romano)</li>
	<li><strong>FCC e-cloud <a href="https://cernbox.cern.ch/index.php/s/48TjBaHlUtS9H5O">buildup movies</a>&nbsp;</strong>(by L. Mether)</li>
</ul>

![png](video_example_0.png)
