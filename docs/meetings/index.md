# Meetings of the working group

---

Agendas, minutes and material from the periodic **meetings of the e-cloud working group** are collected in the following **indico** cathegory:

**https://indico.cern.ch/category/5020/**

![png](indico2.png)