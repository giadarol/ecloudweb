# Software tools

---

<p><strong>Simulation codes:</strong></p>

<ul>
	<li><strong><a href="https://github.com/PyCOMPLETE/PyECLOUD/wiki">PyECLOUD</a>:&nbsp;</strong>used in stand alone mode for e-cloud buildup simulations and in combination with PyHEADTAL to simulate effects on beam dynamics.</li>
	<li><strong><a href="https://github.com/PyCOMPLETE/PyHEADTAIL/wiki">PyHEADTAIL</a>:&nbsp;</strong>used to simulate beam dynamics in the presence of collective effects.</li>
	<li><strong><a href="https://github.com/PyCOMPLETE/PyPIC/wiki">PyPIC</a>:&nbsp;</strong>Collection of Particle-In-Cell solvers.</li>
	<li><strong><a href="https://github.com/PyCOMPLETE/PyPARIS/wiki">PyPARIS</a>:&nbsp;</strong>Parallelization tool for PyECLOUD-PyHEADTAIL simulations.</li>
</ul>

<p><strong>Tools used&nbsp;for machine data anaysis:</strong></p>

<ul>
	<li><strong><a href="https://github.com/PyCOMPLETE/GasFlowHLCalculator">GasFlowHLCalculator</a>:</strong> offline recalculation of the heat loads on the LHC beam screens.</li>
	<li><strong><a href="https://github.com/PyCOMPLETE/hl_dicts">hl_dicts</a></strong><strong>:&nbsp;</strong>used to build synthetic heat load dictionaries for heat load data analysis over long periods.</li>
	<li><strong><a href="https://github.com/giadarol/HeatLoadCalculators">Heat load calculators</a>:&nbsp;</strong>computation of expected heat loads from impedance and synchrotron radiation (based on analytic estimates) and from e-cloud (based on simulations).</li>
	<li><strong><a href="https://github.com/PyCOMPLETE/LHCMeasurementTools">LHCMeasurementTools</a>:&nbsp;</strong>manipulation of measured data from the LHC logging database.</li>
</ul>

<p><strong>Other tools and info:</strong></p>

<ul>
	<li><strong><a href="https://www.evernote.com/shard/s626/nl/120513835/2a999f58-38e6-4088-a462-f2fa09ddd46e?title=Backup%20procedure%20-%20cernbox%20project%20ecloud-simulations">How to backup data on the eos project space</a></strong> (Tool: <strong><a href="https://github.com/PyCOMPLETE/PyECLOUDSimFolderTarArchives">PyECLOUDSimFolderTarArchives</a></strong>)</li>
	<li><strong><a href="https://github.com/PyCOMPLETE/CellStudyInputPyECLOUD">CellStudyInputPyECLOUD</a></strong>: generation of photoemission and magnet input for PyECLOUD simulations.</li>
</ul>

<p>&nbsp;</p>
