# Electron Cloud Working Group

CERN - Beams Department - Accalerators and Beam Physics Group

---

This website collects useful information for the **Electron Cloud Working Group** at **[CERN](https://home.cern/)**, concerning simulation and experimental studies for CERN accelerators (**[PS](https://home.cern/science/accelerators/proton-synchrotron)**, **[SPS](https://home.cern/science/accelerators/super-proton-synchrotron)**, and **[LHC](https://home.cern/science/accelerators/large-hadron-collider)**), their updgrade projects (**[LIU](https://espace.cern.ch/liu-project/default.aspx)**, **[HL-LHC](http://home.cern/topics/high-luminosity-lhc)**) and design studies for future accelerator facilities (**[CLIC](http://home.cern/about/accelerators/compact-linear-collider), [FCC](http://home.cern/about/accelerators/future-circular-collider)**).

In the website section it is possible to find

*  Agendas, minutes and material from the **[working group meetings](meetings)**
*  Material on **[e-cloud studies (organized by topic)](documents_and_slides)**
*  Notes on e-cloud related **[follow-up of the LHC operation](lhc_followup)**
*  **[Software tools](software_tools)** used for e-cloud studies

![png](pinch.png)

---

The page is based on [mkdocs](https://gitlab.cern.ch/authoring/documentation/mkdocs) + [gitlab](https://gitlab.cern.ch/).

Useful information on how to create and maintain a similar website is available [here](http://bblumi.web.cern.ch/how-tos/#how-to-make-such-a-nice-website).

