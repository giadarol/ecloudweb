
        <tr>
            <td><strong>General information</strong></td>
            <td>G. Arduini</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Measurement of collimator block irradiated samples - status and plans</strong></td>
            <td>N. Biancacci</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Short update on the difference between old and new impedance model</strong></td>
            <td>N. Mounet</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Short update on the HL-LHC octupole and TeleIndex requirement with the new impedance model</strong></td>
            <td>X. Buffat</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Beam-beam induced crabbing</strong></td>
            <td>X. Buffat</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Tune separation along the bunch train</strong></td>
            <td>C. Zannini</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Beam stability with e-cloud for bunch intensities below 2.3e11</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Round table</strong></td>
            <td></td>
            <td>Presentation at HL-LHC WP2 meeting on 21 Apr 2020 (<a href="https://indico.cern.ch/event/903324/">indico page</a>)</td>
        </tr>
    