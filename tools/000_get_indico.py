import requests
import json
import datetime

import indico

meeting_name = 'e-cloud meeting'
categid = 5020
eventid = 811014
eventid = 830024
eventid = 835473
eventid = 840676
eventid = 862794
eventid = 862798
eventid = 877137
eventid = 895303
eventid = 948277
eventid = 962349
eventid = 977568

categid = 0
meeting_name = 'HL-LHC Annual meeting 2020'
eventid = 937797
meeting_name = 'HSC Section meeting'
eventid = 952122
eventid = 904845
eventid = 919193
eventid = 883740
meeting_name = 'LHC MD day'
eventid = 867177
meeting_name = 'LHC Run 3 configuration meeting'
eventid = 902528
meeting_name = 'HL-LHC WP2 meeting'
eventid = 903324
eventid = 924097
eventid = 967065
eventid = 961360
meeting_name = 'ABP Information meeting'
eventid = 880340
eventid = 926256
eventid = 974183
# meeting_name = 'ABP injectors WG meeting'
# eventid = 968538

event = indico.Event.from_id(categid, eventid)

table_entries = ''
for cc in event.contributions:
	speakers_string = ' and '.join (
		[f'{ss.first_name[:1]}. {ss.last_name}' for ss in cc.speakers])

	dateobj = datetime.datetime.strptime(cc.startDate['date'], '%Y-%m-%d')

	date_str = dateobj.strftime('%d %b %Y')

	table_entry = f'''
        <tr>
            <td><strong>{cc.title}</strong></td>
            <td>{speakers_string}</td>
            <td>Presentation at {meeting_name} on {date_str} (<a href="{event.url}">indico page</a>)</td>
        </tr>
    '''

	table_entries += table_entry

with open(f'categ_{categid}/tabentries_event{eventid}.txt', 'w') as fid:
	fid.write(table_entries)
