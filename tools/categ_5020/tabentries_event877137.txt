
        <tr>
            <td><strong>Arising matters</strong></td>
            <td>G. Iadarola and G. Rumolo</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Development of WARP simulations for 3D RF structures</strong></td>
            <td>L. Giacomel</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Ion energy spectrum for the LHC arcs</strong></td>
            <td>L. Mether</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Analysis of measurements from the LHC Vacuum Pilot Sector</strong></td>
            <td>E. Buratin</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Laser treatments for SEY reduction</strong></td>
            <td>M. Himmerlich</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>AOB</strong></td>
            <td>. everybody</td>
            <td>Presentation at e-cloud meeting on 24 Jan 2020 (<a href="https://indico.cern.ch/event/877137/">indico page</a>)</td>
        </tr>
    