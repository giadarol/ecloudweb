
        <tr>
            <td><strong>Arising matters</strong></td>
            <td>G. Iadarola and G. Rumolo</td>
            <td>Presentation at e-cloud meeting on 20 Aug 2019 (<a href="https://indico.cern.ch/event/840676/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Effect of e-cloud magnetic field on proton and electron motion</strong></td>
            <td>L. Giacomel</td>
            <td>Presentation at e-cloud meeting on 20 Aug 2019 (<a href="https://indico.cern.ch/event/840676/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>Follow-up on Furman-Pivi simulations</strong></td>
            <td>G. Iadarola</td>
            <td>Presentation at e-cloud meeting on 20 Aug 2019 (<a href="https://indico.cern.ch/event/840676/">indico page</a>)</td>
        </tr>
    
        <tr>
            <td><strong>AOB</strong></td>
            <td>. everybody</td>
            <td>Presentation at e-cloud meeting on 20 Aug 2019 (<a href="https://indico.cern.ch/event/840676/">indico page</a>)</td>
        </tr>
    